"""
Get to know the most common and basic object types
and terminology
"""

## Integer values

a = 1  # This is an "assignment". The object a is created and assigned the value 1

b = 100
c = a + b
c

## Overwrite variables
a = 5
c

c = a + b
c

## Function to check the type of a variable:
# parentheses / round brackets used for functions

type(c) # "int" is the abbreviation for integer


## Use meaningful names for variables

width = 20
height = 2*5

width*height


## Floating points

radius = 10
pi = 3.141592653589793

area = pi * radius**2 

type(area)

## Delete a variable
del area

area    # no longer available

## "null" variable
a = None


## Strings
name = "Ulrich"

# Can also have spaces, etc.
randomString = "have a good week"

type(randomString)

name + randomString

name + " " + randomString

# ' and " are interchangable
name + ' ' + randomString


# combine '' and ""
message = "Enter value '5'"
print(message)
