"""
Simple, "complex" objects.
"""


### Lists ###
# create with square brackets

ls1 = [1, 2, 4, 5]
ls1

# 'list' is the function to convert other 'list-like' objects to lists
ls2 = list(range(0,10))

# therefore never call your object 'list'
list = [1, 2, 4, 5]
ls2 = list(range(0,10))  # this now raises an error, because 'list' is now a list
del list
ls2 = list(range(0,10))  # after deleting the list 'list' the function 'list' works again


# btw. 'ls1' is allowed as an object name while '1ls' is not
1ls = [1,2,3]

names_ls = ["Horst", "Ahmad", "Maria", "Nuri"]

names_ls

# accessing lists

names_ls[1]

names_ls[0] # NOTE: in Python numbering starts from 0

names_ls[-1] # the last item of a list
names_ls[-2] # the second-last item of the list

# access several elements
names_ls[0:3]   # all elements from position "0" to "3" (not included)

names_ls[1:4:2] # every second element starting from position "1"


# mixed lists are allowed
mixed_list = [1, "random text", names_ls]

mixed_list

# data types are kept
type(mixed_list[0])
type(mixed_list[1])
type(mixed_list[2])


# join lists
a = [10,11,12]
b = [3,4,5]

c = a + b
c


## some useful functions
# the length of a list
len(a)

# append a list
a.append(13)

# sort lists
c.sort()
c

names_ls.sort(reverse = True)
names_ls


### Tuples ###
# A tuple is a collection which is ordered and unchangeable.

tp1 = (1, 2, 4, 5)
tp1

names_tp = ("Horst", "Ahmad", "Maria", "Nuri")

# the main difference to lists: tuples cannot be modified
names_ls[1] = "Paul"
names_ls

names_tp[1] = "Paul"

# list of tuples with first and last names

fullnames = [("Ulrich", "Kreidenweis"), ("Andres de Jesus", "Vargas Soplin")]

fullnames[0]

firstnames = [fullnames[0][0], fullnames[1][0]]
firstnames



### Sets ###
# Set items are unordered, unchangeable, and do not allow duplicate values.

set1 = {"apple", "banana", "cherry", "apple"}

set2 = {"banana", "cherry", "cherry"}   # They are useful if dublicates should be removed

set2[2] # this doesn't work, because the order is not defined.

# sets are also useful to identify the difference between list-like objects
set1.difference(set2)
