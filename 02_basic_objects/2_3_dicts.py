"""
Dictionaries store data in key:value pairs.
The keys are used to access the values
"""

## Store data for one cow
# spaces before keys are not needed here, but help to make the code look nice
# do not confuse with sets, which also use {-} brackets
cow1 = {'id': 1,
        'name': "Lisa",
        'dob': "2020-02-03", 
        'weight_kg': 521}

# other way to construct using the dict function (less common)
cow1 = dict(id1 = 1, name = "Lisa", dob = "2020-02-03", weight_kg = 521)


# Access values by keys (strings)
cow1["name"]
cow1["weight_kg"]


# Advantage of dicts: data on one physical object (e.g. cow) stored in one object in Python
print("cow", cow1['name'], "weights",  cow1['weight_kg'], "kg")

# more elegant with f-string, where code in between {} is evaluated
print(f"cow {cow1['name']} weights {cow1['weight_kg']} kg")


## Other option to get values: the get function
cow1.get("name")

# Difference in error management
cow1["sex"]
cow1.get("sex")
type(cow1.get("sex")) # the get function returns None for missing keys

## Change values
# Overwrite entries: Dictionary is a mutable object (like list, and unlike tuple)
cow1["weight"] = 550

# += and -= options: Increase and decrease values
cow1["weight"] += 10
cow1

cow1["weight"] += 10
cow1

cow1["weight"] -= 50
cow1

# add a new key value pair
cow1["sex"] = "female"

# get all available keys: Really helpful if you have a dict, and cannot remember how it looks
cow1.keys()

# all values
cow1.values()

# combinations of key and values
cow1.items()



## Nested dictionaries
# Dictionaries as entries (values) of other dictionaries

cow2 = {'id': 2,
        'name': "Maria",
        'dob': "2017-05-12", 
        'weight_kg': 634}

# Nested dictionary of single cow dicts with ids as keys
cows = {"id1": cow1, "id2": cow2}

cows["id1"]["name"]


## Defining calves

calf1 = {'id': 10,
        'name': "Heidi",
        'dob': "2021-03-20", 
        'weight_kg': 230}

calf2 = {'id': 11,
        'name': "Gerlinde",
        'dob': "2022-03-25", 
        'weight_kg': 125}

# Calves dictionaries are entry of cow dictionary
cow2 = {'id': 2,
        'name': "Maria",
        'dob': "2017-05-12", 
        'weight_kg': 634,
        'calves' : {"id10": calf1, "id11": calf2}}

# Create the cows dictionary once more
cows = {"id1": cow1, "id2": cow2}

# access the entries
cows.keys()
cows["id2"]
cows["id2"].keys()
cows["id2"]["calves"]
cows["id2"]["calves"].keys()
cows["id2"]["calves"]["id11"]
cows["id2"]["calves"]["id11"]["name"]
