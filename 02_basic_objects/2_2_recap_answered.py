"""
Recapitulate what we have learned so far
"""

## Simple objects ##

# Task 1
# Calculate the surface area of a spheric/ball, using the formula: 4*π*r²
# π is 3.1416
# The radius is 12
# Define π and the radius as seperate variables first
# Store the result in a variable called "surface_area"

radius = 12
pi = 3.1419
surface_area = 4 * pi * radius**2
surface_area

# Task 2
# Check of what type the calculated surface area is

type(surface_area)

# Task 3
# Define a message that says: "The surface area is: *result"
# *result stands for the variable that you created before
# print this message

message = "The surface area is: " + str(surface_area) + " cm²"
print(message)

# or:
print("The surface area is:", surface_area, "cm²")



## Lists ##

# Task 4
# Create a list called 'numbers' with values from 10 to 1, so 10, 9, 8 ... 1

numbers = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]

# or:
numbers = list(range(10, 0, -1)) # The last parameter (-1) in this case defines the step, which is -1 in this case

# Task 5
# Sort this list so that is starts with 1 and ends with 10

numbers.sort()
numbers

# or
numbers.reverse()   # if you run both functions (sort and reverse), it will be as originally
numbers

# Task 6
# Add the number 11 at the end of the list

numbers.append(11)
numbers

# or:
numbers + [11]

# Task 7
# Create a list of even numbers (called even_numbers), by selecting every second element of the numbers list.
# The result should be 2, 4, 6, 8, 10

even_numbers = numbers[1:11:2]  # selection goes like [first element, second element, step]

even_numbers = numbers[1:len(numbers):2]

# Task 8
# Create a list of odd numbers by checking for the difference of numbers and even_numbers

odd_numbers = list(set(numbers).difference(set(even_numbers)))