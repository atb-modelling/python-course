"""
Recapitulate what we have learned so far
"""

## Simple objects ##

# Task 1
# Calculate the surface area of a spheric/ball, using the formula: 4*π*r²
# π is 3.1416
# The radius is 12
# Define π and the radius as seperate variables first
# Store the result in a variable called "surface_area"


# Task 2
# Check of what type the calculated surface area is


# Task 3
# Define a message that says: "The surface area is: *result"
# *result stands for the variable that you created before
# print this message



## Lists ##

# Task 4
# Create a list called 'numbers' with values from 10 to 1, so 10, 9, 8 ... 1


# Task 5
# Sort this list so that is starts with 1 and ends with 10


# Task 6
# Add the number 11 at the end of the list


# Task 7
# Create a list of even numbers (called even_numbers), by selecting every second element of the numbers list.
# The result should be 2, 4, 6, 8, 10


# Task 8
# Create a list of odd numbers by checking for the difference of numbers and even_numbers

