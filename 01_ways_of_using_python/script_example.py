# Example python script
# Lines behind a "#" are comments and not executed

from datetime import datetime

print("Starting the script")

## ask for name
name = input('Enter your first name:\n')

print('\nHello', name,"!")

## report current time
print("Do you want to know the current time?")

isTimeRequested = input("If so enter 'y', otherwise 'n':\n")

# if else construction, depending on input
if isTimeRequested == "y":   # Check whether input is equal to "y"
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    print("It is now", current_time)
elif isTimeRequested == "n":
    print("Ok. Have a good day then.")
else:
    print("Sorry, unexpected input")