"""
Some examples for good code
"""


## Use function annotations
# See PEP 3107 https://peps.python.org/pep-3107/

# Function that creates dictionaries for cows
def create_cow_dict(name, id, **kwargs):
    return {"name": name,
            "id": id,
            **kwargs}

# Function annotations
# default values are still possible

def create_cow_dict(name: str, id: int = 1, **kwargs) -> dict:
    return {"name": name,
            "id": id,
            **kwargs}

# Now, during typing you can see what input is exected and what is returned by the function
create_cow_dict(name = "Heidi")

create_cow_dict(name = 1) # No error


## Docstrings

def create_cow_dict(name: str, id: int = 1, **kwargs) -> dict:
    """Returns a dictionary of important cow data"""
    return {"name": name,
            "id": id,
            **kwargs}

