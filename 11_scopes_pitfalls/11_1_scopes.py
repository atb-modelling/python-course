"""
Different scopes: Where Python looks for objects
"""


## When defining objects, this usually happens in the global scope
a = 1

# dictionary of global variables
globals()

# global variables are accessible within function
def testfun():
    print(a)

testfun()

# local variables which are defined within functions do not change global variables

def testfun2():
    a = 2
    print(a)

testfun2()
a

# local variables are not accessible outside
def testfun3():
    b = 3
    print(b)

testfun3()
b   # not defined (which is indicated by the red underscoring in VS Code)



## Python gives priority to more local variables

x = 10
def wrapperfun():
    x = 20
    def innerfun():
        print(x)
    innerfun()

wrapperfun()


## possiblity to change global variables within functions with global

y = 33
def changeglobalfun():
    global y
    y = 44

y
changeglobalfun()
y