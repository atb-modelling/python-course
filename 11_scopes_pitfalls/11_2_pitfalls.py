"""
Learn about tricky pitfalls: assignments of mutable and immutable objects
"""

## assigment works differently for mutable and immutable objects
x = 10
y = x
y
y = 11
x       # as expected
y 

a = [10, 20]
b = a
b[0] = 11
b
a   # a has changed as well

# the same happens for dicts and pandas
import pandas as pd

Cows_1 = pd.DataFrame({'weight':[600, 620]}, index=["Heidi","Lisa"])
Cows_1
Cows_2 = Cows_1

Cows_2.loc["Heidi","weight"] = 1000
Cows_1



## a look under the hood
# the id() function returns a unique identification of the object in the computer memory
x = 10
id(x)
y = x
id(y)   # at this point, x and y are the same object in memory

y = 11
id(y)   # a new object has been created

# a and b are actually the same object. b points to object a
a = [10, 20]
id(a)
b = a
id(b)
b[0] = 11
id(b)   # a and b remain to be the same object


# 'is' will return True if two variables point to the same object (in memory)
x = 10
y = x
x is y


## One way to avoid the problem: copy() and deepcopy() functions

Cows_1 = pd.DataFrame({'weight':[600, 620]}, index=["Heidi","Lisa"])

Cows_2 = Cows_1.copy()
id(Cows_1)
id(Cows_2)

Cows_2.loc["Heidi","weight"] = 1000
Cows_1

# deepcopy for nested data
# from dicts example

calf1 = {'id': 10,
        'name': "Heidi",
        'dob': "2021-03-20", 
        'weight_kg': 230}

calf2 = {'id': 11,
        'name': "Gerlinde",
        'dob': "2022-03-25", 
        'weight_kg': 125}

cow2 = {'id': 2,
        'name': "Maria",
        'dob': "2017-05-12", 
        'weight_kg': 634,
        'calves' : {"id10": calf1, "id11": calf2}}

cow2copy = cow2.copy()
id(cow2)
id(cow2copy) # these are two different objects now

id(cow2["calves"]["id10"])
id(cow2copy["calves"]["id10"])  # these are still identical


from copy import deepcopy
cow2deepcopy = deepcopy(cow2)
id(cow2["calves"]["id10"])
id(cow2deepcopy["calves"]["id10"])  # these are now different