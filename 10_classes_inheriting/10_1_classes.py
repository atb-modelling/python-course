"""
Classes are the best of object oriented programming.
They combine data and functions.
"""

## We have worked with classes before

# list class
ls = [1, 2, 3]
type(ls)


# classes usually have methods (functions) and attributes (variables)

# dir function shows all available methods and attibutes
dir(ls) # there are quite many methods: all of this form __xyz__ are internal, and are usually not used directly

# reverse is an examples of a method
ls.reverse()    # reverse the list
ls

# the __repr__ methods defines how the output should be shown in the terminal
ls.__repr__()


# DataFrame is a class of pandas
import pandas as pd

pd.DataFrame    # control + left mouseclick brings you to the code. Do not modify!

df = pd.DataFrame({'A': [100, 150], 'B': [110, 120]}, index=["first", "second"])

dir(df) # even more methods and properties

# example of a methods
df.transpose()

# example of an attribute (that in this case does the same thing)
df.T

# other examples of attributes and methods
df.index
df.values

df.isna()
df.max(axis="columns")


## Define our own class
# "class" is the keyword to define classes
# by convention use names starting with capital letter
# functions within classes are called 
# all classes need a __init__ method, that is called when initializing the class
# self is a reference to the current instance of the class

class Cow:                                      # define the class
    def __init__(self, name, weight_kg):        # define arguments with which the class is initated
        self.name = name                        # make these parameters properties of the class
        self.weight_kg = weight_kg

Cow     # this is only the class
Cow()   # this initates the class. This is complaining because we did not define required arguments

Cow1 = Cow(name="Lisa", weight_kg=600)

Cow1    # This is an instance of a class, so one specific 'parameterization' of this class

# Similar for lists:
a = [1,3,7] # a is an instance of class a


# Access class attributes
Cow1.name
Cow1.weight_kg


# So far not much better than dictionaries. Let's add a function (method)
# And define default arguments

class Cow:

    FEED_UTILIZATION = 0.5  # class (not instance) dependent parameters / attributes, can be defined here

    def __init__(self, name="undefined", weight_kg=600):    # default arguments
        self.name = name
        self.weight_kg = weight_kg
        self.fed_count = 0

    def feed(self, fodder_kg):
        self.weight_kg += fodder_kg / self.FEED_UTILIZATION    # very simple cow weight gain function :-)
        self.fed_count += 1



# One more function

from datetime import date

class Cow:

    FEED_UTILIZATION = 0.5  # class (not instance) dependent parameters / attributes, can be defined here

    def __init__(self, name="undefined", weight_kg=600, dob=date(2000, 1, 1)):
        self.name = name
        self.weight_kg = weight_kg
        self.dob = dob

    def feed(self, fodder_kg):
        self.weight_kg += fodder_kg / self.FEED_UTILIZATION
        self.fed_count += 1

    def age_in_days(self):
        return (date.today() - self.dob).days


Cow2 = Cow(name="Marie", weight_kg=600, dob=date(2019,5,3))
Cow2.age_in_days()



## The __repr__ function: Decide how class instances are printed

# so far, if we call a class instance it is showing us no information
Cow2

class Cow:

    FEED_UTILIZATION = 0.5  # class (not instance) dependent parameters / attributes, can be defined here

    def __init__(self, name="undefined", weight_kg=600, dob=date(2000, 1, 1)):
        self.name = name
        self.weight_kg = weight_kg
        self.dob = dob
    
    def feed(self, fodder_kg):
        self.weight_kg += fodder_kg / self.FEED_UTILIZATION
        self.fed_count += 1

    def age_in_days(self):
        return (date.today() - self.dob).days

    def __repr__(self):
        return f"Cow {self.name} with {self.weight_kg} kg"


Cow3 = Cow(weight_kg=600, name="Marie")
Cow3


## Exercise:
# In a separate file (humans.py):
# Create a class called 'Student':
# Define it with one argument 'name'
# Also create an empty list 'grades' at initialization
# Define two methods:
# 1. A method called 'add_grade' that has one argument 'grade', which is added to the 'grades' list
# 2. A method 'avg_grade' that returns the average from the list 'grades' (rounded to two digits)


# NOTE:  Solution in --> humans.py

# make sure that you are in the correct folder
import os
os.getcwd()

# import the class from the different file
from humans import Student      # already now the class attribure is defined (therefore, don't add complex things to class attributes)

Student.human

g = Student(name="George")

g.grades

g.add_grade(1.7)
g.grades

g.add_grade(1)
g.add_grade(1.3)

g.grades

g.avg_grade()