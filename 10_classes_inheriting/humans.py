## Exercise:
# In a separate file (humans.py):
# Create a class called 'Student':
# Define it with one argument 'name'
# Also create an empty list 'grades' at initialization
# Define two methods:
# 1. A method called 'add_grade' that has one argument 'grade', which is added to the 'grades' list
# 2. A method 'avg_grade' that returns the average from the list 'grades' (rounded to two digits)


class Student:

    print("Class attribute is defined")
    human = True    # Something that is universally true for all students.

    def __init__(self, name):
        print("Instance attributes are defined")
        self.name = name
        self.grades = []

    def add_grade(self, grade):
        self.grades.append(grade)

    def avg_grade(self):
        if len(self.grades) == 0:
            return 0
        else:
            return round(sum(self.grades) / len (self.grades), 2)