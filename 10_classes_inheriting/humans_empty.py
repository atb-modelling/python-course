## Exercise:
# In a separate file (humans.py):
# Create a class called 'Student':
# Define it with one argument 'name'
# Also create an empty list 'grades' at initialization
# Define two methods:
# 1. A method called 'add_grade' that has one argument 'grade', which is added to the 'grades' list
# 2. A method 'avg_grade' that returns the average from the list 'grades' (rounded to two digits)