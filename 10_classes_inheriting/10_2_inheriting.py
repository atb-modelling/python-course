"""
Inherit functionality across classes
"""


## Assume we want to define cows, pigs and sheep
# They will all share some similarities
# So we can save some effort

from datetime import date

class FarmAnimal:
    def __init__(self, name, weight_kg):
        self.name = name
        self.weight_kg = weight_kg

    def feed(self, fodder_kg):
        self.weight_kg += fodder_kg


# Define a Sheep class, that inherits all the functionality from FarmAnimal
class Sheep(FarmAnimal):
    pass

S1 = Sheep(name="Shaun", weight_kg=120)

S1.feed(fodder_kg=5)
S1.weight_kg


# Adding class specific methods is possible
class Pig(FarmAnimal):
    
    def killable(self):
        if self.weight_kg >= 100:
            return True
        else:
            return False

P1 = Pig(name="Arnold", weight_kg=80)

P1.killable()

P1.feed(fodder_kg=10)

P1.killable()

P1.feed(fodder_kg=10)

P1.killable()


# Maybe we don't want to give pigs names, but ids
# Overwrite __init__

class Pig(FarmAnimal):
    def __init__(self, id):
        self.id = id

P2 = Pig(id=101, weight_kg=90) # weight_kg parameter is lost

# whole _init__ has to be defined new
class Pig(FarmAnimal):
    def __init__(self, id, weight_kg):
        self.id = id
        self.weight_kg = weight_kg

P2 = Pig(id=101, weight_kg=90)

P2.feed(fodder_kg=20)
