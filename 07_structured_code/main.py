########################################
# This is our main code execution file #
########################################

## make sure that you are in the correct folder

import os
os.getcwd()

import pandas as pd
from matplotlib import pyplot as plt

# Import functions from your own modules (subfolders)
from analysis import avg_daily_milk_yield
from plotting.lineplots import milkyieldplot


## read in the data
dat = pd.read_csv("data\milkyields.csv", index_col=0)
dat.index = pd.to_datetime(dat.index)

## process the data using some own defined function
dat["average"] = avg_daily_milk_yield(dat, decimals=3)

type(avg_daily_milk_yield(dat, decimals=3))

# plot the so created data
milkyieldplot(dat)

# and show it on screen
plt.show()


# NOTE:
# If changes are made to a module that has been imported already
# it has to be reloaded with: importlib.reload(*modulename)
# or, alternatively just kill the terminal (often easier)
