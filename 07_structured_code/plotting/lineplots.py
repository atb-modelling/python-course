##############################
# Collection of my lineplots #
##############################

import pandas as pd
from matplotlib import pyplot as plt



def milkyieldplot(data: pd.pandas):
    """Subplots for each cow (id_# column) that compares it to the average"""

    if not "average" in data.columns:
        raise Exception("column 'average' required")

    id_columns = [c for c in data.columns if "id_" in c]

    fig, axs = plt.subplots(len(id_columns))
    fig.suptitle('Milk Yields')

    for i, id in enumerate(id_columns):
        axs[i].plot(data.loc[:,[id, "average"]])
        axs[i].set_title(id)
        axs[i].set_ylim([20, 30])

        if i < len(id_columns)-1:
            axs[i].set_xticks([], [])


    plt.xticks(rotation=45)
    plt.ylabel("daily milk yield [l]")
    plt.legend(["cow","mean"], ncol=2, loc="upper center")
    fig.tight_layout()

    return fig