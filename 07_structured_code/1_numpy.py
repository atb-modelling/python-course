## A file with the name of a module may cause problems
# if the current working directory is the folder containing the file

# Therefore do NOT name your files like official python packages (like numpy.py)
# Something like 1_numpy.py should be fine.

import os
os.getcwd()

import numpy as np

np.array([1,2,3])