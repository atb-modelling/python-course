##############################
# Code to create sample data #
##############################


import numpy as np
import pandas as pd

dates = ["2023-03-" + f"{i:02d}" for i in range(1,32)]

yield_101 = np.random.uniform(low=20, high=25, size=31).round(2)
yield_102 = np.random.uniform(low=24, high=26, size=31).round(2)
yield_103 = (np.random.uniform(low=21, high=23, size=31) + np.arange(0, 3.1, 0.1)).round(2)

MilkYields = pd.DataFrame(
    {"id_101": yield_101,
     "id_102": yield_102,
     "id_103": yield_103},
     index=dates)

MilkYields.to_csv("milkyields.csv")