#########################################
# Functions to do some analysis on data #
#########################################

import pandas as pd

def avg_daily_milk_yield(data: pd.DataFrame, decimals: int) -> pd.Series:
    """
    Average milk yields of all cows per day
    :type data: pandas data
    :param data: Dataframe of daily milk yields. Different cows in columns, dates in rows
    :type decimals: int
    :param decimals: Round the average to this number of decimals
    :return: Average milk yields. Dates as rows (as in original)
    :rtype: pd.Series
    """
    return data.mean(axis = 1).round(decimals)