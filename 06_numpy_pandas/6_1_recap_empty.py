"""
Create some numpy arrays
"""

## Task 1:
# Create a one dimensional array (called 'vect') with values from 100 to 92: [100, 99, 98, ... 92]


## Task 2
# Subtask 2.1.
# Create a matrix (called 'brightness1'): The first row should hold the values [255, 128, 0], the second [80, 12.0, 255], the third [1, 45, 50]


# Subtask 2.2
# Create the same matrix (this time called 'brightness2'), but using the datatype uint8


# Subtask 2.3
# Use the .nbytes attribute, to see whether the total number bytes consumed by the elements of 'brightness2' is lower than by 'brightness1'



## Task 3
# Convert the vector from task 1 ('vect') to a 3 x 3 matrix called 'm'



## Task 4
# Do a element-wise matrix multiplication of 'brightness 2' and 'm'