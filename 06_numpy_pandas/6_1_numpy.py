"""
Numpy: n-dimensional arrays of data
usefull for:
- math, e.g. matrix multiplication
- efficient manipulation of larger datasets: e.g. logger data, raster data, etc.
"""

## the default way of importing numpy
import numpy as np

## the core part of numpy: arrays

# one dimensional array (vector)

a = np.array([2, 3, 4])
a

b = np.array([1.2, 3, 5.1])     # mixture of float and integers
b

# get the data type of these arrays
a.dtype

type(b[1])  # this element is also converted to a float object (numpy float)
b.dtype # the whole array is of type float64 (64 bit)

# different to list, numpy arrays have only one data type
c = [1.2, 3, 5.1]
type(c[1])  # here 3 would remain an int

# define data type manually
a2 = np.array([2, 3, 5], dtype=np.float64)
a2.dtype


## larger arrays
# np.arange, simiar to range function

np.arange(0, 16, 2) # to get numpy array
list(range(0,16,2)) # to get list

# but also working with floating point numbers: 

np.arange(0.1, 16, 0.5)
list(range(0.1, 16, 0.5)) # fails


## difference to lists: multidimensionality
# 2d arrays = matrices
e = np.arange(15).reshape(3, 5)
e

# create 2d array manually
f = np.array([[1, 2, 3], [4, 5, 6]])

# 3d array with random numbers between 0 and 100
np.random.seed(0)   # this assures that we get the same results every time that we execute the function (results are not really random)

g = np.random.randint(low=0, high=101, size=(3, 4, 5))
g

## basic object information
# overall number of elements
g.size # entries: 3 matrices * 4 rows * 5 columns

# shape
g.shape

# number of dimensions
g.ndim

# mean
g.mean()

# max
g.max()

# min
g.min()


## access the data
# access 2d arrays
f = np.array([[1,2,3], [4, 5, 6]])  # from above
f

f.shape     # two rows (vectors), each of 3 elements

f[0]    # first row
f[0, 2]     # first row, third entry
f[1, 0:2]   # second row, first and second entry
f[:, 2]     # third column

# access 3d arrays
h = np.arange(1,61).reshape(3, 4, 5)
h

h[0,0,0]    # first number of fist dimension

h[2,3,4]    # => see powerpoint

# resolve in steps
h[2]
h[2][3]
h[2][3][4]

h
h[:,:,2]


## some math with numpy arrays

# 1d arrays (vectors)

arr = np.array([5, 9, 7.5])
arr + 1
arr * 2
arr ** 0.5 # square root

# compare this to lists
ls = [5, 9, 7.5]

ls + 1  # fails
ls * 2  # dublicates the list

# 2d arrays (matrices)

arr1 = np.arange(1,10).reshape(3,3)
arr1
arr2 = np.arange(10,19).reshape(3,3)
arr2

# matrix addition
arr1 + arr2

# element-wise product
arr1 * arr2

# dot product
np.dot(arr1, arr2)


# array filled with zeros
np.zeros((2, 3))    # takes the shape as the first argument

# array filled with NaN
nan_arr = np.empty((3,3), dtype=np.float64)
nan_arr[:] = np.NaN     # special object, for "not a number"
nan_arr[1, 1:3] = [64.456, 345.454]  # fill certain values
nan_arr
nan_arr.sum()
np.nansum(nan_arr)


