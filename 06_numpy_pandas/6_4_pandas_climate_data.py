"""
Using pandas and functions on DWD climate data

Recent (last 500 days) values from DWD:
https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/daily/kl/recent/
Description:
https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/daily/kl/recent/KL_Tageswerte_Beschreibung_Stationen.txt

"""

## 1. Example: 
# Figure out the hottest day in year 2022 in Potsdam

## First manually unzip folder

# Figure out current working directory for relative path. Use .chdir() to change it
import os
os.getcwd()

import pandas as pd

# read in the data with pandas read_csv
Climate_Pdm = pd.read_csv("06_numpy_pandas/climate_data/tageswerte_KL_03987_akt/produkt_klima_tag_20210510_20221110_03987.txt")

# columns are separated by semicolons (;)
# help for the read_csv function: https://pandas.pydata.org/docs/reference/api/pandas.read_csv.html
Climate_Pdm = pd.read_csv("06_numpy_pandas/climate_data/tageswerte_KL_03987_akt/produkt_klima_tag_20210510_20221110_03987.txt", sep=";")

# TXK is the daily maximum of temperature (°C) at 2m height -> see description

# one way to access this column
Climate_Pdm["TXK"]

# not working. Why?
Climate_Pdm.columns # space before name

Climate_Pdm = pd.read_csv("06_numpy_pandas/climate_data/tageswerte_KL_03987_akt/produkt_klima_tag_20210510_20221110_03987.txt",
                          sep=";",
                          skipinitialspace=True)

Climate_Pdm.columns

# The maximum temperature measured for Potsdam was
Climate_Pdm["TXK"].max()

# But what is the date?
# Several options:
Climate_Pdm.sort_values('TXK', ascending=False) #  first line is the result

# Or idxmax() which gives the row index
Climate_Pdm["TXK"].idxmax()

# use the row index to access
Climate_Pdm.loc[[Climate_Pdm["TXK"].idxmax()],["MESS_DATUM", "TXK"]]

# Do the same thing for the minimum temperature: "TNK"
Climate_Pdm.loc[[Climate_Pdm["TNK"].idxmin()],["MESS_DATUM", "TNK"]]


# We forgot something: The values are for the last 500 days, therefore the value is from 2021.
# We need to do a subset

# Callenge MESS_DATUM in yearmonthday format

Climate_Pdm["MESS_DATUM"]

# convert to datetime format
Climate_Pdm["date"] = pd.to_datetime(Climate_Pdm["MESS_DATUM"], format="%Y%m%d")

# access the year information (.dt to access datetime formats)
Climate_Pdm["date"].dt.year

# subset
Climate_Pdm2022 = Climate_Pdm[Climate_Pdm["date"].dt.year == 2022]


# finally get the minimum value for 2022
Climate_Pdm2022.loc[[Climate_Pdm2022["TNK"].idxmin()],["MESS_DATUM", "TNK"]]




################################################################################

## 2. Example:
# Automatized hottest and coldest day per month for several cities
# Aim: Excel table, with two sheets, one for Min and one for max temperatures. Listing all the stations

from zipfile import ZipFile

# info on ZipFile library: https://docs.python.org/3/library/zipfile.html

# station_ids = ["03987", "00403"]

# instead, we will use a dict to use the names later on
station_ids = {'03987': "Potsdam",
               '00403': "Berlin-Dahlem"}

# for testing purposes we manually take one value first
# id = "03987"

MinTemp2022City = pd.DataFrame()
MaxTemp2022City = pd.DataFrame()

for id in station_ids:
    print(f"starting loops for city {station_ids[id]}")

    # this time we will use the zipfile directly (without unzipping first)
    archive = ZipFile(f'06_numpy_pandas/climate_data/tageswerte_KL_{id}_akt.zip', 'r')

    # all entries of the zipfile
    entries = archive.namelist()

    # we only want the "product_klima_...txt file"
    # filter with list comprehension
    PRODUCT_FILE_NAME = [e for e in entries if "produkt_klima_tag" in e][0]

    # read from archive
    climate_file = archive.open(PRODUCT_FILE_NAME, mode="r")

    Climate = pd.read_csv(climate_file,
                          sep=";",
                          skipinitialspace=True)

    # convert the "MESS_DATUM" column to a datetime object
    Climate["date"] = pd.to_datetime(Climate["MESS_DATUM"], format="%Y%m%d")
    Climate["year"] = Climate["date"].dt.year
    Climate["month"] = Climate["date"].dt.month

    ## figure out the hottest and coldest day each month
    # TXK is the daily maximum of temperature (°C) at 2m height
    # TNK is the daily minimum of temperature (°C) at 2m height

    # use groupby get aggregate for specified columns
    MaxTemp2022 = Climate.groupby(by=['year', 'month']).max().loc[2022,"TXK"]
    MaxTemp2022 = MaxTemp2022.to_frame().transpose()
    MaxTemp2022.index = [station_ids[id]]   # like this the index has no name
    MaxTemp2022City = pd.concat([MaxTemp2022City, MaxTemp2022])

    MinTemp2022 = Climate.groupby(by=['year', 'month']).min().loc[2022,["TNK"]]
    MinTemp2022.columns = pd.Index([station_ids[id]], name="city")  # like this the index has a name
    MinTemp2022 = MinTemp2022.transpose()
    MinTemp2022City = pd.concat([MinTemp2022City, MinTemp2022])



MinTemp2022City
MaxTemp2022City

## by the way: pandas has an integrated plotting functionality

from matplotlib import pyplot as plt
MinTemp2022City.T.plot(kind="line")
plt.show()


## write to Excel
# I would generally avoid using Excel sheets in combination with programming, because interfaces may change
# Better rely on csv

# for one table very simple:
MinTemp2022City.to_excel('06_numpy_pandas/min_max_data.xlsx', sheet_name="Min Temp")

# for several sheets a ExcelWriter needed
with pd.ExcelWriter('06_numpy_pandas/min_max_data.xlsx') as writer:  
    MinTemp2022City.to_excel(writer, sheet_name="Min Temp")
    MaxTemp2022City.to_excel(writer, sheet_name="Max Temp")