"""
Create some numpy arrays
"""

import numpy as np

## Task 1:
# Create a one dimensional array (called 'vect') with values from 100 to 92: [100, 99, 98, ... 92]

vect = np.arange(100, 91, -1)


## Task 2
# Subtask 2.1.
# Create a matrix (calles 'brightness1'): The first row should hold the values [255, 128, 0], the second [80, 12.0, 255], the third [1, 45, 50]

brightness1 = np.array([[255, 128, 0], [80, 12.0, 255], [1, 45, 50]])


# Subtask 2.2
# Create the same matrix (this time called 'brightness2'), but using the datatype uint8

brightness2 = np.array([[255, 128, 0], [80, 12.0, 255], [1, 45, 50]], dtype=np.uint8)

# alternatively:

brightness2 = brightness1.astype(np.uint8)


# Subtask 2.3
# Use the .nbytes attribute, to see whether the total number bytes consumed by the elements of 'brightness2' is lower than by 'brightness1'

brightness1.nbytes
brightness2.nbytes


## Task 3
# Convert the vector from task 1 ('vect') to a 3 x 3 matrix called 'm'

m = vect.reshape(3, 3)


## Task 4
# Do a element-wise matrix multiplication of 'brightness 2' and 'm'

brightness2 * m
