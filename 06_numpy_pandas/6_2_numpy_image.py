"""
Image manipulation with numpy

Images can be read in as numpy arrays with the three color channels as separate dimensions
"""


## Read in image data

# requires the matplotlib package
# install first if not yet available in cmd Terminal: conda install matplotlib

import numpy as np
from matplotlib import pylab as plt

# Figure out current working directory for relative path.
# Use .chdir() to change it, or open folder in Visual Studio Code
import os
os.getcwd()

orig_img = plt.imread("06_numpy_pandas/image.jpg")

type(orig_img)

orig_img.shape # You can see the resolution: Height, Width, and three color channels RGB (red, green, blue)

## Plot the image
plt.imshow(orig_img)
plt.show()

# red channel
# RGB: red = 0, green = 1, blue = 2
orig_img[:,:,0].min()   # minimal value
orig_img[:,:,0].max()   # maximal value

orig_img.dtype  # unsigned integer (no negative values), with 8-bit

# each color channel can take a value between 0 and 255
# because 8 binary bits can store 256 levels

2**8

128 + 64 + 32 + 16 + 8 + 4 + 2 + 1

format(255, "b")
format(0, "b")
format(1, "b")
format(2, "b")
format(3, "b")
format(4, "b")
format(5, "b")


# histogram
plt.hist(orig_img[:,:,0].flatten(),  bins=50, color="red")
plt.show()


## plot all channels in their colour

fig, axs = plt.subplots(1, 3, figsize=(15,5))
axs[0].imshow(orig_img[:,:,0], cmap="Reds")
axs[1].imshow(orig_img[:,:,1], cmap="Greens")
axs[2].imshow(orig_img[:,:,2], cmap="Blues")

axs[0].set_title("Red Channel")
axs[1].set_title("Green Channel")
axs[2].set_title("Blue Channel")

plt.show()


## Increase the saturation of green

greener_img = orig_img.copy()

# RGB: we increase the green, and decrease the red and blue
greener_img[:,:,0] -= 5
greener_img[:,:,1] += 10
greener_img[:,:,2] -= 5

fig, axs = plt.subplots(1,2)
axs[0].imshow(orig_img)
axs[1].imshow(greener_img)
axs[0].set_title("Original")
axs[1].set_title("Greener version")
plt.show()


# Image got greener, but what has happened?

orig_img[0,0,:] # the pixel in the top left corner was 255 for all colors

greener_img[0,0,:] # by increasing the green color we caused integer overflow

a = np.array([253, 254, 255], dtype=np.uint8)
a
a + 2


## Simple solution

greener_img2 = orig_img.copy()

greener_img2[:,:,1][greener_img2[:,:,1] < 245] += 10
greener_img2[:,:,0][greener_img2[:,:,0] > 5] -= 5
greener_img2[:,:,2][greener_img2[:,:,2] > 5] -= 5


fig, axs = plt.subplots(1,2)
axs[0].imshow(orig_img)
axs[1].imshow(greener_img2)
axs[0].set_title("Original")
axs[1].set_title("Greener version")
plt.show()