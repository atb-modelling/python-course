"""
pandas are very useful table objects
general info on pandas under https://pandas.pydata.org/docs/index.html

- based on numpy
- but with named indices
"""

import pandas as pd

# Easily create pandas from dicts
cow1 = {'id': "id_101",
        'name': "Lisa",
        'dob': "2020-02-03", 
        'weight_kg': 521}

## two different pandas types:

# 1. Series, which only have one named dimension, similar to dicts
pd.Series(cow1)


# 2. DataFrames, which have two dimensions
# created from lists
pd.DataFrame({'name': ["Lisa", "Maria"],
             'dob': ["2020-02-03", "2017-05-12"],
             'weight_kg': [521, 634]
             })

# or by several dicts
cow2 = {'id': "id_102",
        'name': "Maria",
        'dob': "2017-05-12", 
        'weight_kg': 634}

Cows = pd.DataFrame([cow1, cow2])
Cows = pd.DataFrame([cow1, cow2], index=["id_101","id_102"])



## Access by name with .loc
# df.loc[row, column]
Cows.loc["id_102","name"] # one element is returned in the original format, here as string

# Access data from several columns
Cows.loc["id_102",["name", "weight_kg"]] # one-dimensional objects are returned as pd.Series

# Access all columns: ":" stands for all entries
Cows.loc["id_102", :] # pd.Series returned

# Access all rows
Cows.loc[:, "name"]

# Keep the data as DataFrame
Cows.loc[["id_102"],:]

# Access with boolean
Cows.loc[:,[False,False,True,False]]


## Access by dimension number with .iloc
# The first row
Cows.iloc[0,:]

# The last column
Cows.iloc[:,-1]

## Short form to get columns (without .loc)
Cows["name"]
Cows.name   # only working if column names have no space


## Pandas of milk yields

MilkYields = pd.DataFrame({"id_101": [22, 26],
                           "id_102": [21, 25.5]},
                           index=["2023-01-24","2023-01-25"])

MilkYields

# Getting the same if we had a numpy array before

import numpy as np

MilkYieldsArr = np.array([[22, 26], [21, 25.5]])   # milk yields
MilkYieldsArr

pd.DataFrame(MilkYieldsArr)     # rows and columns not correct

# transpose
MilkYieldsArr.transpose()

# or short form
MilkYieldsArr.T

pd.DataFrame(MilkYieldsArr.T) 

# add index and column names
MilkYields2 = pd.DataFrame(MilkYieldsArr.T, index=["2023-01-24","2023-01-25"], columns=["id_101","id_102"])


# transpose also works on pandas
MilkYields2.T   # like this the result is not saved to a new object

# compare elements
MilkYields2 == MilkYields

# check whether both dataframes are the same
MilkYields.equals(MilkYields2)

MilkYields.dtypes   # because both columns are still of different type
MilkYields2.dtypes      # MilkYields2 was created from numpy where all elements had the same type

MilkYields = MilkYields.astype("float64")

MilkYields.equals(MilkYields2)


## Modify dataframe

# First add missing info for one more cow (as a list)
MilkYields["id_103"] = [20, 20.5]

MilkYields.dtypes  # note that the different columns have different data types

# Add the current value
MilkYields.loc["2023-01-26", :] = [18, 27, 20]

# Relace some incorrect value
MilkYields.loc["2023-01-25", "id_101"] = 20.7

## Subsetting

# all rows where value for id_101 is bigger than 20
MilkYields[MilkYields.id_101 > 20]
# same as:
MilkYields[MilkYields["id_101"] > 20]

# only the respective column
MilkYields["id_101"][MilkYields["id_101"] > 20]
# alternatively:
MilkYields.loc[MilkYields["id_101"] > 20, "id_101"]     # output is a pd.Series

MilkYields.loc[MilkYields["id_101"] > 20, ["id_101"]]     # if we want a pd.Dataframe


# Get the rows where the milk yield of id_101 is higher than if id_102
MilkYields[MilkYields["id_101"] > MilkYields["id_102"]]
# alternatively
MilkYields.query('id_101 > id_102')


# Increase value for all cows by 2 at one date
MilkYields.loc["2023-01-26", :] += 2

# Total over time
MilkYields.loc["total",:] = MilkYields.sum(axis=0)

# Average milk yield per day
MilkYields["average"] = MilkYields.mean(axis=1)

# If we would want to exclude remove the "average" column again
MilkYields = MilkYields.drop(columns="average")


## Add two dataframes of same format together
MilkYieldsFeb = pd.DataFrame({"id_101": [22, 26],
                              "id_102": [21, 25.5],
                              "id_103": [19, 19.5]},
                              index=["2023-02-01","2023-02-02"])

# without the total column
MilkYields.loc[MilkYields.index != "total", :]

pd.concat([MilkYields.loc[MilkYields.index != "total", :] , MilkYieldsFeb])


## Some math with two pandas

# Total milk yield per kg of bodymass
MilkYields.loc["total",:] / Cows["weight_kg"]

# Milk yield per kg of bodymass
MilkYields / Cows["weight_kg"]