"""
Test your knowledge on if else
"""

# The input function can be used to get user input (from the terminal)

name = input('Enter your name:')    # execute line by line, when used interactively. Enter the name in the terminal
Ulrich
print('Hello, ' + name)


## Task 1:
# Create an algorithm that converts German numerical grades into text form.

# Define a variable called 'grade' that stores the numerical grade value, e.g. 2
# The algorithm should define a variable called 'grade_text' that stores the written text, e.g. "good" depending on the 'grade'
# The following conversion should happen:
# grade 1.0 and 1.3: very good
# all grades between and including 1.7 and 2.3: good
# all grades between and including 2.7 and 4.3: sufficient
# worse than 4.3: insufficient
# for all other cases a message should be shown: "Unable to convert grade"



# Task 2:
# Solve this task in a separate file so that you can run it from the terminal with: python *filename*.py

# Task 2.1
# Create a dictionary (called 'logins') with login data for the following two users:
# 1. name: *YourName*, password: 123456
# 2. name: *TheNameOfYourRightNeighbour*, password: 5LWQfe7H
# Store the names as the key, and the passwords as the values


# Task 2.2
# Create a dictionary (called grades_db) where grades on the latest exam are stored:
# 1. name: *YourName*, grade: 1.3
# 2. name: *TheNameOfYourRightNeighbour*, grade: 5
# Again, use the name as the key, and the grade as the value


# Task 2.3
# Create a login:
# Using the 'input' function, first ask for the name "What is your name?", and store the answer as variable 'name'.
# Then ask for the password "Hello *name*, enter your password". Store this as variable 'password'.
# If the password for the user matches the password from the 'logins' dictionary print "Access granted"
# If the password is incorrect, or the user not in 'logins' print "Access denied"


## Task 2.4
# Expand the previous example:
# If the password is correct, not only show "Access granted" but also the grade for this user from dict 'grades'
# Differentiate two cases:
# If the grade is better or equal than 4.3 print "Your grade is: *the grade from the database*"
# If the grade is worse than 4.3 show the following text: "You failed miserably :-P"