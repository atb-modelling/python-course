"""
Check your knowledge about while and for loops
"""


## Task 1
# Create a list called 'ids' that look like ["id_100", "id_102", "id_104", ..., "id_110"]
# Subtask 1.1
# Do this once using a while loop

id = 100
ids = []

while id <= 110:
    ids.append("id_"+ str(id))
    id += 2 # here we have to increase the id by ourself

ids

# Subtask 1.2
# Do the same thing using a for loop

ids = []

for id in range(100, 111, 2):  # here we first create a list-like object that contains the values 100, 102, 104, ...
    ids.append("id_"+ str(id))

ids