"""
Check your knowledge about while and for loops
"""


## Task 1
# Create a list called 'ids' that look like ["id_100", "id_102", "id_104", ..., "id_110"]
# Subtask 1.1
# Do this once using a while loop


# Subtask 1.2
# Do the same thing using a for loop