"""
Booleans and if else

## Python supports the following logical conditions:

a == b    # a equal to b
a != b    # not equal
a < b     # a less than b
a <= b    # less or equal
a > b     # greater than
a >= b    # greater or equal

"""

# Examples

1 == 1
1 == 2
"apple" == "apple"
"apple" == "orange"
"apple" != "orange"

1.0 == 1
1.0001 == 1

2 > 1
2 > 2
2 >= 2


type(2 > 1) # the return object is of type 'boolean'

# either True or False
True
False


## if else constructions

a = 10

if a > 5:
    print("a is bigger than 5")  # this line needs to be indented with four spaces (VS Code automatically creates four spaces if you press the tab key)

a = 1  # run the previous again with a set to 1


# if else example:
# Check whether temperature in barn is too high. If so turn on the fan.

current_temp = 20
TEMP_THRESHOLD = 25

if current_temp >= TEMP_THRESHOLD:
    print("turning on fan")
    # <= some function that would turn on the fan here
else:
    print("temperature not too high")  # this line is executed in case the previous if condition is not fulfilled (False)


# combine booleans with 'and' and 'or'

True and True
True and False
True or False
False or False

current_humidity = 0.80
HUMIDITY_THRESHOLD = 0.70

if current_temp >= TEMP_THRESHOLD or current_humidity >= HUMIDITY_THRESHOLD:
    print("turning on fan")
    # <= some function that would turn on the fan here
else:
    print("temperature or humidity are not too high")


# negate booleans with 'not'

not False

# expand the previous example. Only turn the fan if it is not already running

fan_running = False

if (current_temp >= TEMP_THRESHOLD or current_humidity >= HUMIDITY_THRESHOLD) and not fan_running:
    print("turning on fan")
    # <= some function that would turn on the fan here
    fan_running = True  # we now change the status of the fan
elif fan_running:
    print("fan is already running")     # this is an additional condition that is only executed if the previous is not fulfilled
else:
    print("temperature or humidity are not too high")

# try the previous example again (the fan is now already running)


# boolean interpretation of integers and None

if 0:
    print("test")

if 10:
    print("test")

bool(10) # function to check the boolean representation of an object
bool("apple")
bool(None)

0 == False
1 == True

a = None
if a:
    print("test")


## other useful options

# check whether list contains an element
nbr = 10
ls = [1, 3, 6, 10]

nbr in ls


# example: grant access to the milking robot

# dict of cow milking
cows_to_milk = {'Lisa' : True,
               'Maria' : False}

name = "Lisa" # assume this info is received by a sensor

if cows_to_milk[name]:
    print("Access granted")
    # <= some function to open gate here
    cows_to_milk[name] = False
else:
    print("Access denied")


# previous example with
name = "Maria"
name = "Heidi"  # KeyError, because 'Heidi' is not on the list


# one solution: first check whether the cow is belonging to the group of cows generally allowed.
if name in cows_to_milk:      # cows_to_milk.keys() would also work
    # then check whether it has not been milked already
    if cows_to_milk[name]:   
        print("Access granted")
        # <= some function to open gate here
        cows_to_milk[name] = False
    else:
        print("Acces denied")
else:
    print("Access denied")

name = "Maria" # execute previous


# shorter, simpler (better) solution
cows_to_milk['Lisa'] = True # reset value for Lisa to True

if cows_to_milk.get(name):      # get("Heidi") returns None, and this is interpreted as False
    print("Access granted")
else:
    print("Access denied")

name = "Heidi"
name = "Lisa"


## 'pass' to do nothing
# useful (among others) during development

if cows_to_milk.get(name):
    # NOTE: Develop the code here later on
else:
    print("Access denied")


# pass avoids the error
if cows_to_milk.get(name):
    pass        # NOTE: Develop the code here later on
else:
    print("Access denied")


## using 'isinstance' to check type
# This time we are using the id to grant access

cow_id = "18"

if cow_id in range(0, 101):      # range(0,101) is equal to a list from 0 to 100
    print("Access granted")
else:
    print("Access denied")


# not working, because cow_id was given as a string
# show an error message

if not isinstance(cow_id, int):       # isinstance(x, int) checks whether x is of type int. Note that actually a class int is passed to the function not a string 'int'
    print(f"cow_id needs to be of type 'int'")
elif cow_id in range(0,100):
    print("Access granted")
else:
    print("Access denied")