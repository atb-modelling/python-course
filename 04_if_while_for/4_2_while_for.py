"""
While and for loops
"""

## While loops
# execute as long as a condition is fulfilled

i = 1

while i < 6:
    print(i)
    i += 1  # Here we increase the value by 1

# Make sure that condition is False at some point, otherwise the loop would run forever
# Interrupt the code with Control + C

i = 1

while i < 6:
    print(i)

# Run a function that approaches 1 until a certain difference is reached

x = 0
difference = 1
PRECISION_THRESHOLD = 0.01  # constants in all capital letters

while difference > PRECISION_THRESHOLD:
    y = 1 - 1 / (x + 1)
    print("y: ", y)

    x += 1
    print("x: ", x)

    difference = 1 - y
    print("difference: ", difference, "\n")


## For loops
# Iterate over a sequence

# with predefined lists
cows = ["Lisa", "Maria", "Heidi"]

for c in cows:
    print(c)

# range of values
for i in range(0, 11):
    print("The current value of i is:", i)


# Access several entries at once
# Example create lists of titles, firstnames and lastnames

fullnames = [("Dr.", "Ulrich", "Kreidenweis"), ("M.Sc.", "Andres", "Vargas")]

titles = []
firstnames = []
lastnames = []

for t, f, l in fullnames:
    titles.append(t)
    firstnames.append(f)
    lastnames.append(l)

titles
firstnames
lastnames

# implicitly this happens:
t, f, l = fullnames[0] # in first iteration
t
t, f, l = fullnames[1] # in second iteration


## Reverse a dictionary

Cows = {'Lisa': "id_1",
        'Maria': "id_2",
        'Heidi': "id_3"}

CowsInvert = {}

for k, v in Cows.items():   # here k stands for key, v for value
    CowsInvert[v] = k

# same as before, but just named differently
for name, id in Cows.items():
    CowsInvert[id] = name

Cows
CowsInvert