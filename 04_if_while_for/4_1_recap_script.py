"""
Task 2 of exercise

Small script that contains:
- Logging in with password
- Print user's grade from database (dictionary)

This script can be executed in a command prompt by typing the following code:
python 4_1_recap_script.py
"""


logins = {'Ulrich': "123456",
         'Andres': "5LWQfe7H"}

grades_db = {'Ulrich': 1.3,
             'Andres': 5}


name = input("What is your name?\n")
password = input(f"Hello {name}, enter your password.\n")

if password == logins.get(name):
    print("Access granted")
    user_grade = grades_db[name]
    if user_grade <= 4.3:
        print(f"Your grade is {user_grade}")   
    else:
        print("You failed miserably :-P")

else:
    print("Access denied")