"""
Test your knowledge on dictionaries and importing
"""

## Dictionaries ##

# Task 1
# Create a dictionary called personal_data that contains some information about yourself:
# First name
# Last name
# ATB Phone number

personal_data = {"firstname": 'Ulrich',
                 "lastname": 'Kreidenweis',
                 "phone (office)": '03315699219'}    # NOTE: even though it is only digits it needs to be a string, because of the leading 0

# Task 2
# Add your e-mail address to this dictionary

personal_data["email"] = "ukreidenweis@atb-potsdam.de"

# Task 3
# Create a tuple using the data from the dictionary that contains your first and last name
# like so: ("First Name", "Last Name")

(personal_data["firstname"], personal_data["lastname"])


# Task 4
# Take the last tree digits of your phone number from the dictionary and store as variable direct_dialing
direct_dialing = personal_data["phone (office)"][-3:]
direct_dialing


# Task 5
# Calculate the square root from this three-digit number (direct_dialing)
# using the sqrt function from the math package

from math import sqrt

sqrt(direct_dialing)    # fails, because we defined it as a string
sqrt(int(direct_dialing))


# Bonus task (we haven't discussed the solution yet)
# Try to extract the part before the @ sign of your email address from the dict
# Store it as a variable called login
# Tip: You can use the split function for this purpose

email = personal_data["email"]

login = email.split("@")[0]