"""
Basic math
Import existing packages (example of 'math').

math package website for help: https://docs.python.org/3/library/math.html
"""


## Basic math

1 + 2
2 * 5
2.1 * 5

# float division
10 / 5
11 / 5
11.1 / 2.2

# floor division rounds down result to integer
10 // 2
11 // 2

# exponent
2**3


## Using the math package

# define pi manually
pi = 3.14


## Import the math package
import math

# Some packages offer help
# exit the help by typing: q
help(math)


# available functions
dir(math)

math.pi     # this is the pi value (not a function!) from the math package

math.atan(10)    # arc tangent of 10, in radians

# import directly (often preferably), but may conflict with other defined variables
from math import pi

pi # pi now refers to the value from the math package and no longer to the one manually defined


# import several at once
from math import sin, log10

sin(10)
log10(100)


# import all functions (NOT recommended!)
from math import *

# all functions are now available. No math prefix like math.sqrt(9) needed.
# but danger that your own functions or variables are no longer accessible (see pi example above)

sqrt(9)

# import under different name
import pandas as pd
pd.DataFrame()  # referenced as 'pd' now