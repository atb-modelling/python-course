"""
Test your knowledge on dictionaries and importing
"""

## Dictionaries ##

# Task 1
# Create a dictionary called personal_data that contains some information about yourself:
# First name
# Last name
# ATB Phone number



# Task 2
# Add your e-mail address to this dictionary



# Task 3
# Create a tuple using the data from the dictionary that contains your first and last name
# like so: ("First Name", "Last Name")



# Task 4
# Take the last tree digits of your phone number from the dictionary and store as variable direct_dialing



# Task 5
# Calculate the square root from this three-digit number (direct_dialing)
# using the sqrt function from the math package



# Bonus task (we haven't discussed the solution yet)
# Try to extract the part before the @ sign of your email address from the dict
# Store it as a variable called login
# Tip: You can use the split function for this purpose