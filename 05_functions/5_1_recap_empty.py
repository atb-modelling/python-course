"""
Test your knowledge on functions
"""


## Task 1
# Create a function that returns the temperature humidity index (THI). Call it 'temp_humid_index'
# The two arguments should be 'temp' (for temperature in °C) and 'humid' (for relative humidity in values between 0 and 1, e.g. 0.8 for 80 %)
# The function for the THI is:
# THI = 0.8 * Temp + RelativeHumidity * (Temp - 14.4) + 46.4



## Task 2
# Create another function called 'cow_heat_stress' that takes one argument 'thi' (temperature humidity index)
# It should return the following text (strings) depending on the 'thi' value
# "no stress" (below 72 THI)
# "mild stress" (72 to 79 THI)
# "moderate stress" (80 to 89 THI)
# "severe stress" (90 THI or greater)



## Task 3
# Use the combination of the two functions to check the stress level at
# a temperature of 33°C and 80 % humidity



## Bonus task (Solution not discussed yet):
# Use the two lists of temperature and humidity below to generate a list of THI values

temp_ls = [32, 21, 10]
humid_ls = [0.6, 0.5, 0.9]

# Hint: You can use list comprehension and the zip function to achieve this