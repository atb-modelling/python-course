"""
Where the fun starts: functions

Use functions (among other things) to:
- Avoid repetitions
- Structure code
"""

# We have used two function types before

ls1 = list(range(0, 101, 5))
ls1

## Standalone functions
# length: how many elements has this list?
len(ls1)

# sum
sum(ls1)

# maximum
max(ls1)

# Standalone functions may work with different parameters
tup1 = (1, 5, 2)
max(tup1)   # max function also works on tuples

## Methods: functions which is part of a class, and can only be applied to it
# E.g. the append function comes along with the list class

ls1.append(200)

# or get function from dictionary
d1 = {"x": 10,
      "y": 15}

d1.get("x")

## Defining standalone functions

## Let's program our own division function:
# 'def' is the keyword that is used to define a function
# This is followed by the name of the function
# in brackets '()' are the arguments
# And the line is ended with a doublepoint ':'
# return is what the function is supposed to give back

def division(number, divisor):
    return number / divisor

# positional arguments
division(20, 5)
division(5, 20)

# keyword arguments
division(number=20, divisor=5)
division(divisor=5, number=20)


### Functions without return statement
# If no return is defined, None is returned

## Example: Function manipulates an existing list
# Not considered perfect style in object-oriented programming, because there is no obvious relation between the results list and the function
# Also, the function will fail if we forget to define results before

results = []

def division_to_results(n, d):
    results.append(n / d)

division_to_results(10, 2)
division_to_results(75, 3)
division_to_results(30, 5)

print(division_to_results(30,5))     # Function returns None

results

## Alternative: we could also pass the list to the function

def division_to_list(n, d, result_list):
    result_list.append(n / d)   # result_list here now refers to the object that we will pass to the function


results2 = []

division_to_list(20, 5)   # Now we are reminded that this function requires a list, where it stores the output

division_to_list(20, 5, result_list=results2)
division_to_list(15, 5, result_list=results2)

results2


## Important to understand: Inside the function we refer to the parameter

results = []

def division_to_list(n, d, results):
    results.append(n / d)   # This "results" refers to the parameter, not the results list that we defined before (outside the function)

division_to_list(20, 5, results = results)    # Only here we make the results inside the function access the results outside the function
division_to_list(20, 5, results)    # If we write like this, we are using positional arguments, but are implicitly saying results=results


## Let's use our grade conversion functionality from before
# Storing this as a function has the additional benefit that the function name often helps in understanding the meaning
# Function with several return statements

def grade_conversion(grade):

    if grade == 1 or grade == 1.3:
        # grade_text = "very good"
        return "very good"  # instead of defining a variable we make this directly the return statement

    elif grade >= 1.7 and grade <= 2.3:
        return "good"

    elif grade >= 2.7 and grade <= 4.3:
        return "sufficient"

    elif grade > 4.3:
        return "insufficient"

    else:
        print("Unable to convert grade")

grade_conversion(1)

grade_conversion(3.3)

grade_conversion(5)


## Use the maps function to apply a function to each element of a list
# map takes two arguments: 1. the function, 2. the list that the function is applied on

grades_ls = [1, 3.3, 5]

list(map(grade_conversion, grades_ls))

## Alternatively: List comprehension approach
[grade_conversion(g) for g in grades_ls]


## *args for an undefined number of non-keyword arguments
# using *args is just by convention. Anything else with an asterisk would work, e.g. *add_arguments
# especially useful if we don't know the number of arguments beforehand

def mysum(*args):
    sum = 0
    for n in args:
        sum = sum + n

    return sum

mysum(1, 5, 10)
mysum(3, 6, 8, 12)
mysum()


## pass dictionaries to functions arguments with **
# division function from before

fun_args = {'number': 10,
            'divisor': 5}

division(**fun_args)    # Hint for VS Code: Control + Left Mouseclick on function to move to function definition

# This achieves the same as:
division(number=10, divisor=5)

## Use **kwargs to allow for additional named arguments
# dictionary storing cow data

def create_cow_dict(name, dob, **kwargs):
    return {"name": name,
            "dob": dob,
            **kwargs}

create_cow_dict(name="Heidi", dob="2021-02-01", weight_kg=10)   # possible to add other arguments, that will end up in the dict

create_cow_dict(name="Heidi")   # this fails, and thus assures that all essential data is entered

# Uses combining of two dictionaries
adict = {"A": 100}
bdict = {"B": 500}

{**adict, **bdict}


## using datetime for storing of dates

from datetime import date

# fromisoformat to convert
date.fromisoformat("2021-02-23")

#  previous again with conversion of date to datetime object

def create_cow_dict2(name, dob, **kwargs):
    return {"name": name,
            "dob": date.fromisoformat(dob),
            **kwargs}

Heidi_db = create_cow_dict2(name="Heidi", dob="2021-02-23", weight_kg=10)  # here we add an additional entry, that wasn't defined before

Heidi_db = create_cow_dict2(name="Heidi", dob="2021-02-23", weight_kg=10, breed="Holstein Friesian")

create_cow_dict2(name="Heidi")  # fails and thus assures that we are always defining the neccessary data

Heidi_db["dob"]

# date format comes with several attributes
Heidi_db["dob"].year
Heidi_db["dob"].month
Heidi_db["dob"].day


# strftime to convert from datetime to string of your liking
# docs: https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior
# %Y: Year with century as a decimal number, e.g. 2016
# %m: Month as a zero-padded decimal number, e.g. 01, 02, …, 12
# %d: Day of the month as a zero-padded decimal number, e.g. 01, 02, …, 31

Heidi_db["dob"].strftime("%y/%m/%d")

Heidi_db["dob"].strftime("%Y-%m-%d")