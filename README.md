This material was used as part of a python course for beginners at the Leibniz Institute for Agricultural Engineering and Bioeconomy (ATB) in spring 2023.

For some of the modules training material is available, which is called "xy_recap.py". Then there is usually a version "xy_recap_empty" with just the tasks, and "xy_recap_answered" containing also the solutions.

Authors:
Ulrich Kreidenweis & Andres Vargas